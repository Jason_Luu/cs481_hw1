import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello World App',
      home: Scaffold(
          appBar: AppBar(
            title: Text('Hello World App'),
          ),
          body: Center(
              child: Text('My name is Jason Luu\n'
                  'Expected Graduation Date: May, 2021\n'
                  'Favorite quotes:\n'
                  'Past is experience, Present is experiment and Future is expectation. So use your experience '
                  'in your experiments to achieve your expectation.'
              )
          )
      ),

    );
  }
}

